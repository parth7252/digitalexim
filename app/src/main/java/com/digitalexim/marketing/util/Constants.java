package com.digitalexim.marketing.util;

public class Constants {

    public static String STATUS401 = "401";

    public static class APP_MODE {
        public static String Production = "1";
        public static String Development = "2";
    }

    public static class INTENT {
        public static String From = "from";
        public static String CategoryId = "category_id";
        public static String CategoryName = "category_name";
        public static String Suggestion_Add = "suggestion_added";
        public static String Category_model = "category_model";
        public static String Request_code = "request_code";
        public static String POS = "pos";
        public static String Image_URL = "image_url";
        public static String Business_Id = "business_id";
        public static String fullname = "name";
        public static String email = "email";
        public static String mobile = "mobile";
        public static String fbid = "fbid";
        public static String gbid = "gid";
        public static String profile = "profile";
        public static String CASE_ID = "case_id";
        public static String EVENT_ID = "event_id";
        public static String DOCTOR_ID = "doctor_id";
        public static String SUB_TITLE = "sub_title";
        public static String TITLE = "title";
        public static String ABOUT_ID = "about_id";
        public static String CENTRE_ID = "centre_id";
        public static String SUB_DEPARTMENT_ID = "sub_department_id";
        public static String VIRTUAL_CATEGORY_ID = "virtual_category_id";
        public static String VIRTUAL_TOUR_ID = "virtual_tour_id";
        public static String ACTMESSAGELIST = "ActMessageList";
        public static String ACTReferralHistory = "ActReferralHistory";
        public static String KEYWORD = "keyword";
        public static String FILE_TYPE_TEXT = "text";
        public static String FILE_TYPE_Image = "image";
        public static String SELECTED_DOCTOR = "docotr_selected";
        public static String DATE = "date";
        public static String NOTIFICATION_ID = "notification_id";
        public static String FROM_SIDE_MENU = "side_menu";
        public static String FROM_AFTER_LOGIN = "after_login";

        public static int SELECT_CATEGORY = 3009;
        public static int SELECT_SUB_CATEGORY = 3003;
        public static int SELECT_CITY = 3023;
        public static int SELECT_STATE = 3323;
        public static int SELECT_COUNTRY = 3113;

        public static int REQUEST_PICK_PROFILE_PIC = 3453;
        public static int REQUEST_PICK_BUSINESS_CARD = 3478;
        public static int REQUEST_PICK_KYC = 3018;
        public static int REQUEST_PICK_PHOTO = 39071;

        public static int CAMERA_REQESTCODE_PROFILE_PIC = 39541;
        public static int CAMERA_REQESTCODE_BUSINESS_CARD = 39831;
        public static int CAMERA_REQESTCODE_KYC = 30031;
        public static int CAMERA_REQESTCODE_PHOTO = 35402;
    }

    public static class SHAREDPREFERENCE {

        public static class API {
            public static String DeviceId = "imei_number";   //IMEI
            public static String AndroidToken = "androidtoken";  //push notification device id
            public static String AccessToken = "accesstoken";  //which you get frm login, sociallogin register api
        }

        public static class USER {
            public static String Login = "isLogin";   //0-notLogin & 1-Login
            public static String InternetConnection = "InternetConnection";   //0-notLogin & 1-Login
            public static String ID = "ID";
            public static String Userid = "user_id";
            public static String Emailid = "Emailid";
            public static String FirstName = "first_name";
            public static String LastName = "last_name";
            public static String Salutation = "salutation";
            public static String Photo = "user_photo";
            public static String USER_TYPE = "user_type";
            public static String App_version = "app_version";
            public static String LATITUDE = "latitude";
            public static String LONGITUDE = "longitude";
            public static String show_intro = "show_intro";
            public static String user_category = "user_category";
            public static String category_digital_exim = "category_digital_exim";
            public static String category_import_export = "category_import_export";
        }
    }

    public static class DATETIMEFORMATE {
        //12hr- dd/MM/yyyy hh:mm:ss a   --> Small
        //24hr- dd/MM/yyyy HH:mm:ss     --> Capital

        public static String DDMMM = "dd MMM";
        public static String HHMMA = "hh:mm a";
        public static String YYYYMMDD_HHMMSS = "yyyy-MM-dd HH:mm:ss";
        public static String YYYYMMDD = "yyyy-MM-dd";
        public static String DDMMYYY = "dd-MM-yyyy";
        public static String DDMMMMYYY = "dd MMMM yyyy";
        public static String YYYY = "yyyy";
        public static String MM = "MM";
        public static String DD = "dd";
        public static String HHMMS = "HH:mm:ss";
    }

    //-- Google Analytics Screen Name
    public static class GAI {
        public static String SideMenu = "SideMenu";
    }

    public static class MESSAGES {

        public static class ERROR {
            public static String Unknown = "We are facing some technical issues right now. Please try later";
            public static String NoInternetConnection = "You do not seem to have a strong Internet connection. Kindly move to a WiFi or stronger cellular signal";
            public static String NoRecordsFound = "No records found";
        }
    }
}
