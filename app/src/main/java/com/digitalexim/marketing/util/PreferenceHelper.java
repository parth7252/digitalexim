package com.digitalexim.marketing.util;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {
    private SharedPreferences prefs;

    public PreferenceHelper(Context ctx) {
        prefs = ctx.getSharedPreferences(ctx.getPackageName(), Context.MODE_PRIVATE);
    }

    public boolean isAppLaunched() {
        return prefs.getBoolean("firstRun", false);
    }

    public void setAppLaunched(boolean b) {
        prefs.edit().putBoolean("firstRun", b).apply();
    }

    public boolean isLogin() {
        return prefs.getBoolean("isLogin", false);
    }

    public void setLogin(boolean b) {
        prefs.edit().putBoolean("isLogin", b).apply();
    }

    public String getUserName() {
        return prefs.getString("name", "");
    }

    public void setUserName(String user) {
        prefs.edit().putString("name", user).apply();
    }

    public String getClientName() {
        return prefs.getString("ClientName", "");
    }

    public void setClientName(String clientName) {
        prefs.edit().putString("ClientName", clientName).apply();
    }

    public String getClientPssword() {
        return prefs.getString("ClientPassword", "");
    }

    public void setClientPassword(String clientPassword) {
        prefs.edit().putString("ClientPassword", clientPassword).apply();
    }

    public String getClientId() {
        return prefs.getString("ClientId", "");
    }

    public void setClientId(String user) {
        prefs.edit().putString("ClientId", user).apply();
    }

    public String getGroupName() {
        return prefs.getString("GroupName", "");
    }

    public void setGroupName(String groupName) {
        prefs.edit().putString("GroupName", groupName).apply();
    }

    public String getEmailId() {
        return prefs.getString("email", "");
    }

    public void setEmailId(String EmailId) {
        prefs.edit().putString("email", EmailId).apply();
    }

    public void setUserId(String userId) {
        prefs.edit().putString("user_id", userId).apply();
    }

    public String getUserId() {
        return prefs.getString("user_id", "");
    }

    public String getPhoneNo() {
        return prefs.getString("phone_number", "");
    }

    public void setPhoneNo(String PhoneNo) {
        prefs.edit().putString("phone_number", PhoneNo).apply();
    }

    public String getAuth() {
        return prefs.getString("UserAuth", "");
    }

    public void setAuth(String user) {
        prefs.edit().putString("UserAuth", user).apply();
    }

    public String getTwilioAuth() {
        return prefs.getString("TwilioAuth", "");
    }

    public void setTwilioAuth(String user) {
        prefs.edit().putString("TwilioAuth", user).apply();
    }

    public String getBattery() {
        return prefs.getString("battery", "");
    }

    public void setBattery(String battery) {
        prefs.edit().putString("battery", battery).apply();
    }

    public void setLastSync(String lastSync) {
        prefs.edit().putString("LastSync", lastSync).apply();
    }

    public String getLastSync() {
        return prefs.getString("LastSync", "");
    }

    public String getTodayData() {
        return prefs.getString("todayData", "");
    }

    public void setTodayData(String todayData) {
        prefs.edit().putString("todayData", todayData).apply();
    }

    public boolean getNightMode() {
        return prefs.getBoolean("nightMode", false);
    }

    public void setNightMode(boolean s) {
        prefs.edit().putBoolean("nightMode", s).apply();
    }

    public String getStartTime() {
        return prefs.getString("StartTime", "22:30");
    }

    public void setStartTime(String s) {
        prefs.edit().putString("StartTime", s).apply();
    }

    public String getEndTime() {
        return prefs.getString("EndTime", "08:30");
    }

    public void setEndTime(String s) {
        prefs.edit().putString("EndTime", s).apply();
    }

    public String getSleepData() {
        return prefs.getString("SleepData", "");
    }

    public void setSleepData(String s) {
        prefs.edit().putString("SleepData", s).apply();
    }

    public String getCategory() {
        return prefs.getString("Category", "");
    }

    public void setCategory(String s) {
        prefs.edit().putString("Category", s).apply();
    }

    public void clearAllPrefs() {
        prefs.edit().clear().apply();
    }

    public void setToken(String fcmToken) {
        prefs.edit().putString("token", fcmToken).apply();
    }

    public String getToken() {
        return prefs.getString("token", "");
    }

    public void setImage(String image) {
        prefs.edit().putString("image", image).apply();
    }

    public String getImage() {
        return prefs.getString("image", "");

    }
}
