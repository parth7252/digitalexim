package com.digitalexim.marketing.util;

import android.app.Activity;
import android.app.Dialog;
import android.view.Gravity;
import android.view.View;

import com.digitalexim.marketing.R;

public class PD {

    private Dialog progressDialog;
    private Activity context;

    public PD(Activity context) {
        this.context = context;
    }

    public void createDialog() {
        View view = context.getLayoutInflater().inflate(R.layout.dialog_progress, null);
        progressDialog = new Dialog(context, R.style.DialogSheet);
        progressDialog.setContentView(view);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setLayout(400, 400);
        progressDialog.getWindow().setGravity(Gravity.CENTER);
    }

    public void show() {
        if (progressDialog != null) {
            progressDialog.show();
        }
    }

    public void hide() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
