package com.digitalexim.marketing.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.multidex.MultiDex;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.digitalexim.marketing.R;
import com.digitalexim.marketing.api.ApiFunction;
import com.digitalexim.marketing.api.ApiServices;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orhanobut.hawk.Hawk;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class App extends Application {

    public final String BASE_URL = "http://digitalexim.com/digital_exim/api/";

    private static App singleton;

    private RequestQueue mRequestQueue;

    public static String TAG = "APP";
    static Context context;
    private static App mInstance;
    public static String App_mode = Constants.APP_MODE.Production;  //1 for Production and 2 for Development
    public static String App_plateform = "2";  //1 for IOS and 2 for Android
    public static String strFolderName = ".Focus Fitness";
    public static String strSDCardPath = Environment.getExternalStorageDirectory().toString();
    public static String strFolderFullPath = App.strSDCardPath + File.separator + App.strFolderName;


    public static final String CHANNEL_1_ID = "channel1";
    private static int TIME = 5000;

    static int SCREEN_HEIGHT = 0, SCREEN_WIDTH = 0;

    @Override
    public void onCreate() {
        super.onCreate();

        MultiDex.install(this);
        context = getApplicationContext();
        mInstance = this;

        //-- SharedPreference
        Hawk.init(context).build();




        //-- Permission issue in new phones to strickly get permission
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        createNotificationChannel();

        setRegularFontsAllTxt();

        singleton = this;
    }


    public static synchronized App getInstance() {
        return singleton;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public static App get(Activity activity) {
        return (App) activity.getApplication();
    }


    public void setRegularFontsAllTxt() {
        try {

            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/proximanovacond_regular.ttf")
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //-- Facebook hash key generator
    public static void GenerateKeyHash() {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getApplicationContext().getPackageName(),
                    PackageManager.GET_SIGNATURES); //GypUQe9I2FJr2sVzdm1ExpuWc4U= android pc -2 key
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                App.showLog(TAG, "KeyHash: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //-- Check Internet
    public static boolean isInternetAvail(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }


    //-- Create Folder
    public static void createFolder() {
        FileOutputStream out = null;
        try {
            String directoryPath = App.strFolderFullPath;
            File appDir = new File(directoryPath);
            if (!appDir.exists() && !appDir.isDirectory()) {
                if (appDir.mkdirs()) {
                    App.showLog(TAG, "App Directory created");
                } else {
                    App.showLog(TAG, "Unable To Create App Directory!");
                }
            } else {
                App.showLog(TAG, "App Directory Already Exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //-- Save Bitmap
    public static void saveBitmapSdcard(String filename, Bitmap bitmap) {
        FileOutputStream out = null;
        try {
            String directoryPath = App.strFolderFullPath;

            File appDir = new File(directoryPath);

            if (!appDir.exists() && !appDir.isDirectory()) {
                if (appDir.mkdirs()) {
                    App.showLog("===CreateDir===", "App dir created");
                } else {
                    App.showLog("===CreateDir===", "Unable to create app dir!");
                }
            }


            out = new FileOutputStream(directoryPath + File.separator + filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //-- Bitmap Resize
    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    //-- Hide Keyboard
    public static void hideSoftKeyboardMy(Activity activity, View view) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            //inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //-- Hshow Keyboard
    public static void showSoftKeyboardMy(Activity activity, View view) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);


            //inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    //-- Log
    public static void showLog(String ActivityName, String strMessage) {


        Log.d("From: ", ActivityName + " -- " + strMessage);
    }

    public static void showLogApiRespose(String op, Response response) {
        String strResponse = new Gson().toJson(response.body());
        Log.d("From: =op==>" + op, "response==>" + strResponse);
    }


    //-- Remove Html tags
    public static String stripHtmlRegex(String source) {
        // Replace all tag characters with an empty string.
        return source.replaceAll("<.*?>", "");
    }

    //-- Check Internet
    public static boolean isInternetConnectionAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }



    //-- Snackbar
    public static void showSnackBar(View view, String strMessage) {
        // Toast.makeText(context, ""+strMessage, Toast.LENGTH_SHORT).show();

        try {
            Snackbar snackbar = Snackbar.make(view, strMessage, Snackbar.LENGTH_LONG);
            View snackbarView = snackbar.getView();
            snackbarView.setBackgroundColor(Color.BLACK);
            TextView textView = (TextView) snackbarView.findViewById(R.id.snackbar_text);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //-- Start and End Activity
    public static void myStartActivity(Activity activity, Intent intent) {
        activity.startActivity(intent);
        // activity.overridePendingTransition(0, 0);
        //activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    //-- Start and End Activity
    public static void myStartActivityContext(Context activity, Intent intent) {
        activity.startActivity(intent);
        // activity.overridePendingTransition(0, 0);
        //activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    //-- Start and End Activity
    public static void myStartActivityForResult(Activity activity, Intent intent, int requestcode) {

        activity.startActivityForResult(intent, requestcode);
        // activity.overridePendingTransition(0, 0);
        //activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public static void myStartActivityNoHistory(Activity activity, Intent intent) {
        intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
        activity.startActivity(intent);
        //   activity.overridePendingTransition(0, 0);
        //activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    public static void myStartActivityRefersh(Activity activity, Intent intent) {
        activity.finish();
        activity.startActivity(intent);
        //  activity.overridePendingTransition(0, 0);
    }

    public static void myStartActivityClearTop(Activity activity, Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        activity.finish();
        activity.startActivity(intent);
        //  activity.overridePendingTransition(0, 0);
    }

    public static void myFinishActivityRefresh(Activity activity, Intent intent) {
        activity.finish();
        activity.startActivity(intent);
        //  activity.overridePendingTransition(0, 0);
        //activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    public static void myFinishActivity(Activity activity) {
        activity.finish();
        //  activity.overridePendingTransition(0, 0);
    }


    //-- Valid email
    public static boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}");
        // Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static boolean isValidWebsite(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        return m.matches();
    }

    //-- Valid string
    public static boolean isValidString(String str) {

        if (str != null && str.trim().length() > 0)
            return true;
        else
            return false;

    }


    //-- Px to Dp Converter
    public static int pxToDp(int px, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    //-- Dp to Px Converter
    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }


    //float BANNER_SHOW_PERCENTAGE = 0.40f; // 40%
    public static String dynamicSetSize(Activity activity) {
        String HeightWidth = "";
        try {

            int SCREEN_HEIGHT = 0, SCREEN_WIDTH = 0;

            Display display = activity.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            SCREEN_HEIGHT = size.y;
            SCREEN_WIDTH = size.x;

            App.showLog(TAG, "SCR Height: " + SCREEN_HEIGHT);
            App.showLog(TAG, "SCR Width: " + SCREEN_WIDTH);

            HeightWidth = SCREEN_HEIGHT + ":" + SCREEN_WIDTH;

            return HeightWidth;


        } catch (Exception e) {
            e.printStackTrace();
        }

        return HeightWidth;
    }


    public static String getDateTimeFormate(String strDate, String inputDateFormate, String outputDateFormate) {
        String strFinaldate = "";
        if (strDate != null) {

            try {
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputDateFormate);
                SimpleDateFormat outputFormat = new SimpleDateFormat(outputDateFormate);
                String inputDateStr = strDate;
                Date date = null;
                date = inputFormat.parse(inputDateStr);
                strFinaldate = outputFormat.format(date);
                //strFinaldate = outputFormat.format(date).toLowerCase();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return strFinaldate;
    }


    //-- start Retrofit
    public static OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .retryOnConnectionFailure(true)
                .build();
    }

    public static Retrofit getRetrofitBuilder() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        return new Retrofit.Builder()
                .baseUrl(ApiFunction.strBaseUrl)
                .client(getClient()) // it's optional for adding client
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static ApiServices getRetrofitApiService() {
        return getRetrofitBuilder().create(ApiServices.class);
    }
    //-- end Retrofit

    public static HashMap<String, RequestBody> addCommonHashmap() {

       /* if(Hawk.contains(Constants.SHAREDPREFERENCE.USER.strLid) &&
                Hawk.get(Constants.SHAREDPREFERENCE.USER.strLid).toString().length()>0){


             languageId = Hawk.get(Constants.SHAREDPREFERENCE.USER.strLid);

        }
        else {
            Hawk.put(Constants.SHAREDPREFERENCE.USER.strLid, Constants.SHAREDPREFERENCE.USER.strDefaultLanguageId);
        }
*/
        HashMap<String, RequestBody> commonHashMap = new HashMap<>();

        try {

            commonHashMap.put(ApiFunction.PARM_APPMODE, App.createPartFromString(App.App_mode));
            commonHashMap.put(ApiFunction.PARM_PLATFORM, App.createPartFromString(App.App_plateform));


            App.showLog(TAG, ApiFunction.PARM_APPMODE + ":" + App.App_mode);
            App.showLog(TAG, ApiFunction.PARM_PLATFORM + ":" + App.App_plateform);
            if (Hawk.contains(Constants.SHAREDPREFERENCE.USER.Login) &&
                    Hawk.get(Constants.SHAREDPREFERENCE.USER.Login).toString().equalsIgnoreCase("1") &&
                    Hawk.contains(Constants.SHAREDPREFERENCE.USER.Userid) &&
                    Hawk.get(Constants.SHAREDPREFERENCE.USER.Userid).toString().length() > 0) {

                commonHashMap.put(ApiFunction.PARM_USERID, App.createPartFromString(Hawk.get(Constants.SHAREDPREFERENCE.USER.Userid).toString()));
                commonHashMap.put(ApiFunction.PARM_ACCESSTOKEN, App.createPartFromString(Hawk.get(Constants.SHAREDPREFERENCE.API.AccessToken).toString()));
                commonHashMap.put(ApiFunction.PARM_ANDROIDTOKEN, App.createPartFromString(Hawk.get(Constants.SHAREDPREFERENCE.API.AndroidToken).toString()));

                commonHashMap.put(ApiFunction.PARMS_IO, App.createPartFromString(Hawk.get(Constants.SHAREDPREFERENCE.USER.USER_TYPE).toString()));

                try {
                    commonHashMap.put(ApiFunction.PARMS_LATITUDE, App.createPartFromString(Hawk.get(Constants.SHAREDPREFERENCE.USER.LATITUDE).toString()));
                } catch (Exception e) {
                    commonHashMap.put(ApiFunction.PARMS_LATITUDE, App.createPartFromString(""));
                    e.printStackTrace();
                }
                // commonHashMap.put(ApiFunction.PARMS_LATITUDE, App.createPartFromString(""));
                // commonHashMap.put(ApiFunction.PARMS_LATITUDE, App.createPartFromString(Hawk.get(Constants.SHAREDPREFERENCE.USER.LATITUDE).toString()));

                try {
                    commonHashMap.put(ApiFunction.PARMS_LOGITUDE, App.createPartFromString(Hawk.get(Constants.SHAREDPREFERENCE.USER.LONGITUDE).toString()));
                } catch (Exception e) {
                    commonHashMap.put(ApiFunction.PARMS_LOGITUDE, App.createPartFromString(""));
                    e.printStackTrace();
                }

                //commonHashMap.put(ApiFunction.PARMS_LOGITUDE, App.createPartFromString(Hawk.get(Constants.SHAREDPREFERENCE.USER.LONGITUDE).toString()));
                //commonHashMap.put(ApiFunction.PARMS_LOGITUDE, App.createPartFromString(""));

                try {
                    commonHashMap.put(ApiFunction.PARMS_DEVICE_INFO, App.createPartFromString(Hawk.get(Constants.SHAREDPREFERENCE.API.DeviceId).toString()));
                } catch (Exception e) {
                    commonHashMap.put(ApiFunction.PARMS_DEVICE_INFO, App.createPartFromString(""));
                    e.printStackTrace();
                }
                // commonHashMap.put(ApiFunction.PARMS_DEVICE_INFO, App.createPartFromString(Hawk.get(Constants.SHAREDPREFERENCE.API.DeviceId).toString()));
                //commonHashMap.put(ApiFunction.PARMS_DEVICE_INFO, App.createPartFromString(""));
                commonHashMap.put(ApiFunction.PARMS_APP_VERSION, App.createPartFromString(Hawk.get(Constants.SHAREDPREFERENCE.USER.App_version).toString()));

                App.showLog(TAG, ApiFunction.PARM_USERID + ":" + Hawk.get(Constants.SHAREDPREFERENCE.USER.Userid));
                App.showLog(TAG, ApiFunction.PARM_ACCESSTOKEN + ":" + Hawk.get(Constants.SHAREDPREFERENCE.API.AccessToken));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return commonHashMap;
    }


    public static RequestBody createPartFromString(String value) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), value);
    }

    public static String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    public static void showLogParameters(String key, String parameter) {
        Log.d(key, parameter);
    }

    public static void showLogResponce(String strFrom, String strMessage) {
        Log.d("From: " + strFrom, " strResponse: " + strMessage);
    }


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel3 = new NotificationChannel(
                    CHANNEL_1_ID,
                    "channel1",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel3.setDescription("This is channel 1");

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel3);

        }
    }


    public static String getddMMMMyyyyWithoutTime(String convert_date_string) {
        String final_date = "";
        if (convert_date_string != null) {

            try {
                SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
                String inputDateStr = convert_date_string;
                Date date = null;
                date = inputFormat.parse(inputDateStr);
                //String outputDateStr = outputFormat.format(date);
                final_date = outputFormat.format(date);
                final_date.toLowerCase();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return final_date;
    }

    public static String convertFormat(String inFormat, String opFormat, String data_time) {
        String x = "";
        try {
            DateFormat f1 = new SimpleDateFormat(inFormat);
            Date d = null;
            try {
                d = f1.parse(data_time);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            DateFormat f2 = new SimpleDateFormat(opFormat);
            x = f2.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return x;
    }


    // for the location
    public class LocationConstants {
        public static final int SUCCESS_RESULT = 0;

        public static final int FAILURE_RESULT = 1;

        public static final String PACKAGE_NAME = "com.callkundali";

        public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

        public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
        public static final String RESULT_LOCALITY_KEY = PACKAGE_NAME + ".RESULT_LOCALITY_KEY";

        public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

        public static final String LOCATION_DATA_AREA = PACKAGE_NAME + ".LOCATION_DATA_AREA";
        public static final String LOCATION_DATA_CITY = PACKAGE_NAME + ".LOCATION_DATA_CITY";
        public static final String LOCATION_DATA_STREET = PACKAGE_NAME + ".LOCATION_DATA_STREET";
        public static final String LOCATION_ADDRESS = PACKAGE_NAME + ".LOCATION_ADDRESS";


    }



    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }



    public static boolean checkValidateEmail(String email) {
        Pattern pattern = Pattern
                .compile("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}");
        // Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static int dynamicSetSizeBaseAct(Activity activity) {
        try {
            Display display = activity.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            SCREEN_HEIGHT = size.y;
            SCREEN_WIDTH = size.x;

            App.showLog(TAG, "SCR Height: " + SCREEN_HEIGHT);
            App.showLog(TAG, "SCR Width: " + SCREEN_WIDTH);



        } catch (Exception e) {
            e.printStackTrace();
        }
        return SCREEN_WIDTH;
    }
}

