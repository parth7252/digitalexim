package com.digitalexim.marketing.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class AppConfig {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public static final class URL {
        public static final String BASE_URL = "http://digitalexim.com/digital_exim/api/";
        public static final String URL_LOGIN = "login";
        public static final String URL_REGISTER = "register";
        public static final String URL_PROFILE = "user_profile?user_id=41";
        public static final String URL_FORGET = "forgot_password?email=chintanprajapati74@gmail.com";
        public static final String URL_PURCHASE = "purchase_course";

    }

}
