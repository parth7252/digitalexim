package com.digitalexim.marketing.response;

import com.google.gson.annotations.SerializedName;

public class RespLogin {

    /**
     * status_code : 1
     * message : Login Successfully !
     * data : {"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNDQ3MDNjZDFiM2NkMmM4MTI2MjQxMDE0MWRlODEwNzNlZTEyYWFiZDlkMDIwNWE3NGMwNTQyYjViNDUyZWEwYWZiODRhMDU5OGM3ZWY3MWQiLCJpYXQiOjE1OTQ0ODg1NzUsIm5iZiI6MTU5NDQ4ODU3NSwiZXhwIjoxNjI2MDI0NTc1LCJzdWIiOiI0MSIsInNjb3BlcyI6W119.eNy14_ziBHVZ6QdyvsqFpOUc9BgONv5J9KbGoF0bhmpCb8tKHxp4M0v7q3J5mHKkgxYY--V_LpSE547aipGRsXKiehL4w7Q1Xo1Yr04VAV5nwgRbySdOIlnrYgsCknaE6m55f6j13lTQCt9eemVd33vyW4YTq26ySUhCPvSND5KIEBRddBaTWBci8P0zJv6qtte5SsOtc1scwFuVGjkGAyeKvaTTsMQUPiHxxAbjeLbM3rO472vz8sG8kuoLKHUZr2-hpASS5MUb_zL9BcGuVVXc_urSElJzyvVeKfkpkQ1woCxXXMl50MgyA3Ba5sHxp2EazgJer__HpGGdq-IDdf9fIZjOZ7ksFZTKQ6LxvjQNlp0U23eySkiYqdkY_sjkfxA2EiMAifdXHHrkFI4ccwxwR0aSeMth7dE8qbROXYcObEG5NOYv91U3uteYj3dza1PG8gtzFZGnOk4pxLXRSve6luUfKkKfuDM2jSAKzUtIi-DCs6fPtlj688LzfOdpseztTmGbUTGTtD9R1drkGhLcquWzmv0sPXEBNT1vQlaQhjKQ2IbxB9wN8UvtJre2s184VFCwS5jZI1yJC2DZ-xMx7_zAaBgNDezOcpQmW-Q_SZ-wlreemxnm9T1QpqJM3d73geL_crNKoFOQVT-PGheJVOM856FjjUfTWwz8syM","name":"chintan","image":"http://digitalexim.com/digital_exim/users_image/no_user.png","email":"chintanprajapati74@gmail.com","phone_number":"1254789632","city":"nadiad"}
     */

    @SerializedName("status_code")
    private int statusCode;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataEntity data;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public static class DataEntity {
        /**
         * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNDQ3MDNjZDFiM2NkMmM4MTI2MjQxMDE0MWRlODEwNzNlZTEyYWFiZDlkMDIwNWE3NGMwNTQyYjViNDUyZWEwYWZiODRhMDU5OGM3ZWY3MWQiLCJpYXQiOjE1OTQ0ODg1NzUsIm5iZiI6MTU5NDQ4ODU3NSwiZXhwIjoxNjI2MDI0NTc1LCJzdWIiOiI0MSIsInNjb3BlcyI6W119.eNy14_ziBHVZ6QdyvsqFpOUc9BgONv5J9KbGoF0bhmpCb8tKHxp4M0v7q3J5mHKkgxYY--V_LpSE547aipGRsXKiehL4w7Q1Xo1Yr04VAV5nwgRbySdOIlnrYgsCknaE6m55f6j13lTQCt9eemVd33vyW4YTq26ySUhCPvSND5KIEBRddBaTWBci8P0zJv6qtte5SsOtc1scwFuVGjkGAyeKvaTTsMQUPiHxxAbjeLbM3rO472vz8sG8kuoLKHUZr2-hpASS5MUb_zL9BcGuVVXc_urSElJzyvVeKfkpkQ1woCxXXMl50MgyA3Ba5sHxp2EazgJer__HpGGdq-IDdf9fIZjOZ7ksFZTKQ6LxvjQNlp0U23eySkiYqdkY_sjkfxA2EiMAifdXHHrkFI4ccwxwR0aSeMth7dE8qbROXYcObEG5NOYv91U3uteYj3dza1PG8gtzFZGnOk4pxLXRSve6luUfKkKfuDM2jSAKzUtIi-DCs6fPtlj688LzfOdpseztTmGbUTGTtD9R1drkGhLcquWzmv0sPXEBNT1vQlaQhjKQ2IbxB9wN8UvtJre2s184VFCwS5jZI1yJC2DZ-xMx7_zAaBgNDezOcpQmW-Q_SZ-wlreemxnm9T1QpqJM3d73geL_crNKoFOQVT-PGheJVOM856FjjUfTWwz8syM
         * name : chintan
         * image : http://digitalexim.com/digital_exim/users_image/no_user.png
         * email : chintanprajapati74@gmail.com
         * phone_number : 1254789632
         * city : nadiad
         */

        @SerializedName("token")
        private String token;
        @SerializedName("name")
        private String name;
        @SerializedName("image")
        private String image;
        @SerializedName("email")
        private String email;
        @SerializedName("phone_number")
        private String phoneNumber;
        @SerializedName("city")
        private String city;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }
    }
}
