package com.digitalexim.marketing.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.model.SideMenuModel;

import java.util.List;

public class PopularCourseAdapter extends RecyclerView.Adapter<PopularCourseAdapter.VersionViewHolder> {
    List<SideMenuModel> arrSideMenuItems;
    Context mContext;


    public PopularCourseAdapter(Context context, List<SideMenuModel> arrayListFollowers) {
        arrSideMenuItems = arrayListFollowers;
        mContext = context;
    }

    @Override
    public PopularCourseAdapter.VersionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_popular_course, viewGroup, false);
        PopularCourseAdapter.VersionViewHolder viewHolder = new PopularCourseAdapter.VersionViewHolder(view);
        return viewHolder;
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(final PopularCourseAdapter.VersionViewHolder viewHolder, final int i) {
        try {

            final SideMenuModel modelSideMenu = arrSideMenuItems.get(i);


            viewHolder.tvTitle.setText(modelSideMenu.name);

            viewHolder.ivArrow.setImageDrawable(getDrawableFromID(mContext, modelSideMenu.icon));

            if(i == arrSideMenuItems.size()-1){
                viewHolder.view_line.setVisibility(View.GONE);
            }


            viewHolder.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return arrSideMenuItems.size();
    }


    class VersionViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        LinearLayout llMain;
        ImageView ivArrow;
        View view_line;

        public VersionViewHolder(View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            llMain = (LinearLayout) itemView.findViewById(R.id.llMain);
            ivArrow = (ImageView) itemView.findViewById(R.id.ivArrow);
            view_line = (View) itemView.findViewById(R.id.view_line);

        }

    }

    public static final Drawable getDrawableFromID(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 21) {
            return ContextCompat.getDrawable(context, id);
        } else {
            return context.getResources().getDrawable(id);
        }
    }
}