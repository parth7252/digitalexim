package com.digitalexim.marketing;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.digitalexim.marketing.activity.LoginActivity;
import com.digitalexim.marketing.api.ApiFunction;
import com.digitalexim.marketing.model.CommonResponse;
import com.digitalexim.marketing.model.SideMenuModel;
import com.digitalexim.marketing.util.App;
import com.digitalexim.marketing.util.Constants;
import com.digitalexim.marketing.util.CustomProgressDialog;
import com.digitalexim.marketing.util.NetworkChangeReceiver;
import com.google.gson.Gson;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActBase extends AppCompatActivity {

    protected String TAG = "ActBase";

    protected LinearLayout llFindADoctor, llReferaPatient;
    protected static LinearLayout ll_SubMainContainer, llTopOpt;
    protected RelativeLayout rl_baseToolbar, ll_RightMenu, rlSearch, rlSearchClick, rlBackSearch, rlCloseSearch;


    protected TextView tvTitle, tvYou;
    protected ProgressBar progress_bar;
    protected RelativeLayout rlMenu, rlBack, rlShare;
    protected DrawerLayout drawer;
    protected RelativeLayout left_drawer, rlRightIcons;
    protected EditText edtSearch;


    //Api
    Call callApiMethodBase;
    CustomProgressDialog customProgressDialogBase;

    ///////// Store labels to data base - Localization /////////
    private String[] arrayDBBaseLabels = null;

    private BroadcastReceiver mNetworkReceiver;


    RecyclerView recyclerSideMenu;

    ImageView ivOwnProfilePic, ivBack;
    protected ImageView ivshare;
    TextView tvUserName, tvAppVersion, tvCredit, tvFeedback, tvSettings;

    ArrayList<SideMenuModel> arr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_base);

        baseClickInitialize();
        baseClickEvent();

        getAppVersion();

        Hawk.put(Constants.SHAREDPREFERENCE.USER.InternetConnection, "0");

        //to check internet connection live in app
        mNetworkReceiver = new NetworkChangeReceiver();
        //unregisterNetworkChanges();
//        registerNetworkBroadcastForNougat();
        // permissionEMEI();

        //setSideMenuModel();
        // setSideMenuData();
        hideShowReferApatinet();

      /*  permissionEMEI();
        setPermissionforLocation();*/

        if (App.isInternetAvail(this)) {
            if (Hawk.contains(Constants.SHAREDPREFERENCE.USER.show_intro) &&
                    Hawk.get(Constants.SHAREDPREFERENCE.USER.show_intro).toString().equalsIgnoreCase("1") &&
                    Hawk.contains(Constants.SHAREDPREFERENCE.USER.Userid) &&
                    Hawk.get(Constants.SHAREDPREFERENCE.USER.Userid).toString().length() > 0) {

            }
        }
    }




    private void baseClickEvent() {
        customProgressDialogBase = new CustomProgressDialog(ActBase.this);


        rlBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        rlMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(left_drawer)) {
                    drawer.closeDrawers();
                } else {
                    drawer.openDrawer(left_drawer);


                }
            }
        });


        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                //Called when a drawer's position changes.
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                //  sideMenuAdapter.notifyDataSetChanged();

                App.hideSoftKeyboardMy(ActBase.this, rl_baseToolbar);

                if (Hawk.contains(Constants.SHAREDPREFERENCE.USER.Login) &&
                        Hawk.get(Constants.SHAREDPREFERENCE.USER.Login).toString().equalsIgnoreCase("1") &&
                        Hawk.contains(Constants.SHAREDPREFERENCE.USER.Userid) &&
                        Hawk.get(Constants.SHAREDPREFERENCE.USER.Userid).toString().length() > 0) {

                    try {

                        String user_photo = Hawk.get(Constants.SHAREDPREFERENCE.USER.Photo).toString();

                        if (!TextUtils.isEmpty(user_photo) && !user_photo.equalsIgnoreCase(" ")) {

                            Picasso.get().load(user_photo).error(R.drawable.ic_profile_place_holder).placeholder(R.drawable.ic_profile_place_holder).into(ivOwnProfilePic);

                        }

                        setUSerName();

                    } catch (Exception e) {

                        e.printStackTrace();

                    }

                }

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                // Called when a drawer has settled in a completely closed state.
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                // Called when the drawer motion state changes. The new state will be one of STATE_IDLE, STATE_DRAGGING or STATE_SETTLING.
            }
        });





    }

    private void hideShowReferApatinet() {
        if (Hawk.contains(Constants.SHAREDPREFERENCE.USER.USER_TYPE)) {
            String strio = Hawk.get(Constants.SHAREDPREFERENCE.USER.USER_TYPE).toString();

            if (strio.equalsIgnoreCase("1")) {
                llReferaPatient.setVisibility(View.VISIBLE);
            } else {
                llReferaPatient.setVisibility(View.GONE);
            }
        }
    }

    private void baseClickInitialize() {
        ll_SubMainContainer = (LinearLayout) findViewById(R.id.ll_SubMainContainer);
        ll_RightMenu = (RelativeLayout) findViewById(R.id.ll_RightMenu);
        rl_baseToolbar = (RelativeLayout) findViewById(R.id.rl_baseToolbar);
        rlRightIcons = (RelativeLayout) findViewById(R.id.rlRightIcons);
        rlSearch = (RelativeLayout) findViewById(R.id.rlSearch);
        rlSearchClick = (RelativeLayout) findViewById(R.id.rlSearchClick);
        rlBackSearch = (RelativeLayout) findViewById(R.id.rlBackSearch);
        rlCloseSearch = (RelativeLayout) findViewById(R.id.rlCloseSearch);
        llTopOpt = (LinearLayout) findViewById(R.id.llTopOpt);


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        left_drawer = (RelativeLayout) findViewById(R.id.left_drawer);
        recyclerSideMenu = (RecyclerView) findViewById(R.id.recyclerSideMenu);
        ivOwnProfilePic = (ImageView) findViewById(R.id.ivOwnProfilePic);
        ivBack = (ImageView) findViewById(R.id.ivBack);

        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvAppVersion = (TextView) findViewById(R.id.tvAppVersion);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);


        rlMenu = (RelativeLayout) findViewById(R.id.rlMenu);
        rlBack = (RelativeLayout) findViewById(R.id.rlBack);
        ivshare = (ImageView) findViewById(R.id.ivshare);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        edtSearch = (EditText) findViewById(R.id.edtSearch);

        rlBack.setVisibility(View.GONE);
        rlMenu.setVisibility(View.VISIBLE);

        LinearLayoutManager linearLayoutManagerBase = new LinearLayoutManager(ActBase.this);
        recyclerSideMenu.setLayoutManager(linearLayoutManagerBase);
        recyclerSideMenu.setItemAnimator(new DefaultItemAnimator());
        recyclerSideMenu.setNestedScrollingEnabled(false);

        setEnableDrawer(false);


    }


    public void apiLogout(final boolean gotoHomePage) {
        try {

            if (App.isInternetAvail(ActBase.this)) {
                if (!customProgressDialogBase.isShowing()) {
                    customProgressDialogBase.show();
                }


                HashMap<String, RequestBody> hashMap = new HashMap<>();
                hashMap.putAll(App.addCommonHashmap());


                /*callApiMethodBase = App.getRetrofitApiService().logout(
                        hashMap
                );*/

                App.showLog(TAG, "OP_ : logout");
                for (Map.Entry<String, RequestBody> entry : hashMap.entrySet()) {
                    String key = entry.getKey();
                    RequestBody value = entry.getValue();
                    // do stuff
                    App.showLogParameters(key, " : " + App.bodyToString(value));
                }


                callApiMethodBase.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        try {
                            customProgressDialogBase.dismiss();
                            App.showLog(TAG, "response.raw().request().url();" + response.raw().request().url());


                            CommonResponse responce = response.body();
                            // logoutFinishAllActivity(gotoHomePage);

                            if (responce == null) {

                                App.showLogResponce("--null response--", "==Something wrong=");
                                ResponseBody responseBody = response.errorBody();
                                logoutFinishAllActivity(gotoHomePage);
                                if (responseBody != null) {
                                    try {
                                        App.showLogResponce("------ Api Resonponce Body Null ----------", " -/- " + responseBody.string());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                //200 sucess
                                App.showLog(TAG, "===Response==" + response.body().toString());
                                App.showLogResponce("OP_: ", new Gson().toJson(response.body()));

                                if (responce.status == ApiFunction.strAPITRUE) {
                                    logoutFinishAllActivity(gotoHomePage);
                                } else {
                                    logoutFinishAllActivity(gotoHomePage);
                                    App.showLog(TAG, "------------- API Fails -------------");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        t.printStackTrace();
                        customProgressDialogBase.dismiss();
                        App.showSnackBar(ll_SubMainContainer, Constants.MESSAGES.ERROR.Unknown);
                        App.showLog(TAG, "===main===onFailure=====");
                        logoutFinishAllActivity(gotoHomePage);

                    }
                });


            } else {

                App.showSnackBar(ll_SubMainContainer, Constants.MESSAGES.ERROR.NoInternetConnection);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void logoutFinishAllActivity(boolean gotoHomePage) {
        try {

            String strAndroidToken = Hawk.get(Constants.SHAREDPREFERENCE.API.AndroidToken);


            Hawk.deleteAll();
            Hawk.put(Constants.SHAREDPREFERENCE.API.AndroidToken, strAndroidToken);
            Hawk.put(Constants.SHAREDPREFERENCE.USER.Login, "0");

            if (gotoHomePage) {
                Intent i1 = new Intent(ActBase.this, LoginActivity.class);
                App.myStartActivityRefersh(ActBase.this, i1);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    protected void onResume() {

        super.onResume();
        // getPhoneLanguage();
    }


    public static void getInternetStatus(boolean value) {

        if (value) {
            if (Hawk.get(Constants.SHAREDPREFERENCE.USER.InternetConnection).toString() != null
                    && Hawk.get(Constants.SHAREDPREFERENCE.USER.InternetConnection).toString().length() > 0
                    && Hawk.get(Constants.SHAREDPREFERENCE.USER.InternetConnection).toString().equalsIgnoreCase("0")) {


                Hawk.put(Constants.SHAREDPREFERENCE.USER.InternetConnection, "1");
                //App.showSnackBar(viewShowSnackBar, "Internet Connection Found");
            }


            Handler handler = new Handler();
            Runnable delayrunnable = new Runnable() {
                @Override
                public void run() {
                    //tv_check_connection.setVisibility(View.GONE);
                }
            };
            handler.postDelayed(delayrunnable, 2000); //check every 5second
        } else {
            if (Hawk.get(Constants.SHAREDPREFERENCE.USER.InternetConnection).toString() != null
                    && Hawk.get(Constants.SHAREDPREFERENCE.USER.InternetConnection).toString().length() > 0
                    && Hawk.get(Constants.SHAREDPREFERENCE.USER.InternetConnection).toString().equalsIgnoreCase("1")) {

                Hawk.put(Constants.SHAREDPREFERENCE.USER.InternetConnection, "0");


                App.showSnackBar(ll_SubMainContainer, "You do not seem to have a strong Internet connection. Kindly move to a WiFi or stronger cellular signal.");
            }


        }
    }


    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private void setSideMenuData() {

        if (Hawk.contains(Constants.SHAREDPREFERENCE.USER.Login) &&
                Hawk.get(Constants.SHAREDPREFERENCE.USER.Login).toString().equalsIgnoreCase("1") &&
                Hawk.contains(Constants.SHAREDPREFERENCE.USER.Userid) &&
                Hawk.get(Constants.SHAREDPREFERENCE.USER.Userid).toString().length() > 0) {
            if (Hawk.contains(Constants.SHAREDPREFERENCE.USER.Photo)) {
                String user_photo = Hawk.get(Constants.SHAREDPREFERENCE.USER.Photo).toString();

                if (!TextUtils.isEmpty(user_photo) && !user_photo.equalsIgnoreCase(" ")) {

                    Picasso.get().load(user_photo).error(R.drawable.ic_profile_place_holder).placeholder(R.drawable.ic_profile_place_holder).into(ivOwnProfilePic);

                }
            }

            setUSerName();
        }



    }

    private void setUSerName() {
        String strSalution = "";
        if (Hawk.contains(Constants.SHAREDPREFERENCE.USER.Salutation)) {
            strSalution = Hawk.get(Constants.SHAREDPREFERENCE.USER.Salutation).toString();
        }
        String strFirstName = Hawk.get(Constants.SHAREDPREFERENCE.USER.FirstName).toString();
        String strLastName = Hawk.get(Constants.SHAREDPREFERENCE.USER.LastName).toString();

        if (TextUtils.isEmpty(strSalution)) {
            tvUserName.setText(strFirstName + " " + strLastName);
        } else {
            tvUserName.setText(strSalution + " " + strFirstName + " " + strLastName);
        }


    }

    public void setEnableDrawer(boolean blnEnable) {
        if (blnEnable == true) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }

    }

    public void showLogoutAlert() {
        try {

            //-- Show dialog
            final AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(ActBase.this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(ActBase.this);
            }

            String strMessage = "Are you sure you want to logout?";

            builder
                    .setMessage(Html.fromHtml("<font color='#000000'>" + strMessage + "</font>"))
                    .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // logoutFinishAllActivity(true);
                            apiLogout(true);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

            final AlertDialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(true);
            alertDialog.show();

            Button positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            positiveButton.setTextColor(getResources().getColor(R.color.colorPrimary));

            Button negativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            negativeButton.setTextColor(getResources().getColor(R.color.colorPrimary));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getAppVersion() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;

            tvAppVersion.setText(getString(R.string.str_app_version) + " " + version);

            Hawk.put(Constants.SHAREDPREFERENCE.USER.App_version, "" + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static final Drawable getDrawableFromID(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 21) {
            return ContextCompat.getDrawable(context, id);
        } else {
            return context.getResources().getDrawable(id);
        }
    }


    public static final int getColorFromID(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 21) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }
}
