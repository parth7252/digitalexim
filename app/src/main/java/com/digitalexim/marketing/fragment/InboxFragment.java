package com.digitalexim.marketing.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.activity.DetailsActivity;

public class InboxFragment extends Fragment {

    public static InboxFragment newInstance(int index) {
        InboxFragment fragment = new InboxFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inbox, container, false);


        Button btn_join1 = view.findViewById(R.id.btn_join1);
        Button btn_join2 = view.findViewById(R.id.btn_join2);
        ImageView iv_upcoming = view.findViewById(R.id.iv_upcoming);

        btn_join1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), DetailsActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        btn_join2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), DetailsActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        iv_upcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), DetailsActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        return view;
    }
}
