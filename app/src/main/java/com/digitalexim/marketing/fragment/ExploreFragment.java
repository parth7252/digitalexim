package com.digitalexim.marketing.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.activity.DetailsActivity;
import com.digitalexim.marketing.adapter.PopularCourseAdapter;
import com.digitalexim.marketing.api.GetProfile;
import com.digitalexim.marketing.api.model.GetCourseResponse;
import com.digitalexim.marketing.model.SideMenuModel;
import com.digitalexim.marketing.util.App;
import com.digitalexim.marketing.util.PD;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.razorpay.Checkout;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.digitalexim.marketing.util.App.TAG;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ExploreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExploreFragment extends Fragment {

    private String money = "";
    Call callApiMethod;
    private PD pd;

    RecyclerView recyclerPopularcourse;
    PopularCourseAdapter PopularCourseAdapter;

    public static ExploreFragment newInstance(int index) {
        ExploreFragment f = new ExploreFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        Checkout.preload(getActivity());

         recyclerPopularcourse = view.findViewById(R.id.recyclerPopularcourse);
        Button btn_join1 = view.findViewById(R.id.btn_join1);
        Button btn_join2 = view.findViewById(R.id.btn_join2);
        ImageView iv_upcoming = view.findViewById(R.id.iv_upcoming);
        pd = new PD(getActivity());

        btn_join1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), DetailsActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        btn_join2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), DetailsActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        iv_upcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), DetailsActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });


        getCourseData();
        setPopularCourseAdapter();

        /*AppCompatTextView tv_value_money = view.findViewById(R.id.tv_value_money);

        money = tv_value_money.getText().toString();

        Button btn_payment = view.findViewById(R.id.btn_payment);
        btn_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (money.isEmpty() || money == null) {
                    Toast.makeText(getActivity(), "Payment can't..!!", Toast.LENGTH_SHORT).show();
                }
                else {
                    startPayment(money);
                }
            }
        });*/
        return view;
    }

    private void setPopularCourseAdapter() {

        LinearLayoutManager linearLayoutManagerBase = new LinearLayoutManager(getActivity());
        recyclerPopularcourse.setLayoutManager(linearLayoutManagerBase);
        recyclerPopularcourse.setItemAnimator(new DefaultItemAnimator());
        recyclerPopularcourse.setNestedScrollingEnabled(false);

        try {

            ArrayList<SideMenuModel> arr = new ArrayList<>();

            arr.add(new SideMenuModel("1","Email Marketing", R.drawable.ic_email_marketing,false));
            arr.add(new SideMenuModel("1","Financial Adword", R.drawable.ic_ad_mob,false));
            arr.add(new SideMenuModel("1", "Financial Marketing", R.drawable.ic_financila_marketing,false));

            PopularCourseAdapter = new PopularCourseAdapter(getActivity(), arr);
            recyclerPopularcourse.setAdapter(PopularCourseAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCourseData() {

       /* try {

            if (App.isInternetAvail(getActivity())) {


                        pd.show();




                HashMap<String, RequestBody> hashMap = new HashMap<>();
                hashMap.put("", App.createPartFromString(App.App_mode));


                callApiMethod = App.getRetrofitApiService().getCourseList(hashMap);

                App.showLog(TAG, "OP_ : getImageDetails");
                for (Map.Entry<String, RequestBody> entry : hashMap.entrySet()) {
                    String key = entry.getKey();
                    RequestBody value = entry.getValue();
                    // do stuff
                    App.showLogParameters(key, " : " + App.bodyToString(value));
                }


                callApiMethod.enqueue(new Callback<GetCourseResponse>() {
                    @Override
                    public void onResponse(Call<GetCourseResponse> call, Response<GetCourseResponse> response) {
                        try {

                                    pd.hide();



                            App.showLog(TAG, "response.raw().request().url();" + response.raw().request().url());


                            GetCourseResponse responce = response.body();

                            if (responce == null) {

                                App.showLogResponce("--null response--", "==Something wrong=");
                                ResponseBody responseBody = response.errorBody();

                                if (responseBody != null) {
                                    try {
                                        App.showLogResponce("------ Api Resonponce Body Null ----------", " -/- " + responseBody.string());

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            } else {
                                //200 sucess
                                App.showLog(TAG, "===Response==" + response.body().toString());
                                App.showLogResponce("OP_: ", new Gson().toJson(response.body()));

                                if (responce != null && responce.status != null) {
                                    if (responce.status == ApiFunction.strAPITRUE) {

                                        if (responce.data != null) {
                                            //  lstImage = responce.data;
                                            setIMage(responce.data);
                                        }


                                    } else if (responce.status == ApiFunction.strAPIFALSE) {




                                    } else {

                                    }
                                } else {
                                    App.showLog(TAG, "------------- API Fails -------------");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    @Override
                    public void onFailure(Call<GetCourseResponse> call, Throwable t) {
                        t.printStackTrace();
                        try {

                            pd.hide();

                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }


                        App.showLog(TAG, "===main===onFailure=====");

                    }
                });

            } else {

                // App.showSnackBar(view_pager_indicator, getString(R.string.str_no_internet));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void startPayment(String money) {
        final Activity activity = getActivity();

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Razorpay Corp");
            options.put("description", "Demoing Charges");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", money);

            JSONObject preFill = new JSONObject();
            preFill.put("email", "test@razorpay.com");
            preFill.put("contact", "9876543210");

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }
}
