package com.digitalexim.marketing.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.api.GetProfile;
import com.digitalexim.marketing.util.AppConfig;
import com.digitalexim.marketing.util.PreferenceHelper;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {

    private static final String TAG = ProfileFragment.class.getSimpleName();
    private TextInputEditText et_first_name, et_last_name, et_email, et_phone, et_id_proof;
    private CircleImageView profilepic;
    private String userId = "41", username = "", email = "", city = "", user_image = "", phone_number = "";
    private PreferenceHelper helper;

    public static ProfileFragment newInstance(int index) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        helper = new PreferenceHelper(getActivity());

        userId = helper.getUserId();
        getProfile(userId);

        et_first_name = view.findViewById(R.id.et_first_name);
        et_last_name = view.findViewById(R.id.et_last_name);
        et_email = view.findViewById(R.id.et_email);
        et_phone = view.findViewById(R.id.et_phone);
        et_id_proof = view.findViewById(R.id.et_id_proof);
        profilepic = view.findViewById(R.id.profile_pic);

        Log.d(TAG, "user id : " + userId);
        return view;
    }

    private void getProfile(String userId) {

        HashMap<String, String> params = new HashMap<>();
//        params.put("user_id", userId);
        GetProfile getRequest = new GetProfile(getActivity(), "http://digitalexim.com/digital_exim/api/user_profile?user_id="+ userId, params, new GetProfile.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(JSONObject response) {

                try {
                    JSONArray userDetails = response.getJSONArray("data");
                    Log.d(TAG, "user profile : " + userDetails);
                    Log.d(TAG, "user Profile : " + new GsonBuilder().setPrettyPrinting().create().toJson(response));

                    JSONObject object = userDetails.getJSONObject(0);
                    username = object.getString("username");
                    email = object.getString("email");
                    phone_number = object.getString("phone_number");
                    city = object.getString("city");
                    user_image = object.getString("user_image");
                    Log.d(TAG, "username00 : " + username);

                    et_first_name.setText(username);
                    et_last_name.setText(username);
                    et_email.setText(email);
                    et_phone.setText(phone_number);
                    et_id_proof.setText(userId);

                    Picasso.get().load(user_image).
                            placeholder(R.mipmap.ic_launcher).
                            error(R.mipmap.ic_launcher).
                            into(profilepic);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, false);

        getRequest.doInBackEnd();
    }
}
