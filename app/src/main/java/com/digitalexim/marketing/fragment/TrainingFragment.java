package com.digitalexim.marketing.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.activity.DetailsActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TrainingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TrainingFragment extends Fragment {

    private String money = "";

    public static TrainingFragment newInstance(int index) {
        TrainingFragment f = new TrainingFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_training, container, false);
        /*LinearLayout ll_digital = view.findViewById(R.id.ll_digital);
        LinearLayout ll_web = view.findViewById(R.id.ll_web);

        ll_digital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), DetailsActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        ll_web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), DetailsActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });*/
        return view;
    }
}
