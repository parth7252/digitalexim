package com.digitalexim.marketing.model;

import com.google.gson.annotations.SerializedName;

public class CommonResponse {
    @SerializedName("status")
    public Boolean status;

    @SerializedName("message")
    public String msg;
}
