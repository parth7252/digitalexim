package com.digitalexim.marketing.model;

public class SideMenuModel {
    public String id;
    public String name;
    public int icon;
    public boolean isSelected;

    public SideMenuModel(String id, String name, int icon,boolean isSelected) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.isSelected = isSelected;
    }
}
