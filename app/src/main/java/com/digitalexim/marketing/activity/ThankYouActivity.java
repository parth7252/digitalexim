package com.digitalexim.marketing.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.api.ParamsRequest;
import com.digitalexim.marketing.util.AppConfig;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ThankYouActivity extends AppCompatActivity {

    private static final String TAG = ThankYouActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);

        purchase();
    }

    public void onHome(View view) {
        startActivity(new Intent(ThankYouActivity.this, MainActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();

       // onRestart();
    }

    private void purchase() {
        HashMap<String, String> params = new HashMap<>();
        params.put("course_id", "4");
        params.put("user_id", "34");

        ParamsRequest paramsRequest = new ParamsRequest(this, AppConfig.URL.URL_PURCHASE, params, new ParamsRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(ThankYouActivity.this, error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(JSONObject response) {

                try {
                    JSONArray userDetails = response.getJSONArray("data");
                    Log.d(TAG, "userDetails : " + userDetails);

                    Log.d(TAG, "userDetails : " + new GsonBuilder().setPrettyPrinting().create().toJson(response));


                    LayoutInflater inflater = getLayoutInflater();
                    View view1 = inflater.inflate(R.layout.cust_toast_layout,
                            (ViewGroup) findViewById(R.id.relativeLayout1));

                    TextView tv = view1.findViewById(R.id.textView2);
                    tv.setText(response.getString("message"));
                    Toast toast = new Toast(ThankYouActivity.this);
                    toast.setView(view1);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, true);

        paramsRequest.doInBackEnd();
    }
}
