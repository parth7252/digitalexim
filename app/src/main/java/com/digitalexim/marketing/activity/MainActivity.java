package com.digitalexim.marketing.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.fragment.ExploreFragment;
import com.digitalexim.marketing.fragment.InboxFragment;
import com.digitalexim.marketing.fragment.ProfileFragment;
import com.digitalexim.marketing.fragment.ServicesFragment;
import com.digitalexim.marketing.fragment.TrainingFragment;
import com.digitalexim.marketing.util.PreferenceHelper;
import com.digitalexim.marketing.util.Progress;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemSelectedListener {

    private PreferenceHelper helper;
    private NavigationView navigationView;
    private BottomNavigationView navigation;
    protected Progress progress;
    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        helper = new PreferenceHelper(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigation = findViewById(R.id.navigation);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        navigation.setOnNavigationItemSelectedListener(this);

        progress = new Progress(this);

        /*View header = navigationView.getHeaderView(0);
        txt_user = header.findViewById(R.id.txt_user);
        txt_number = header.findViewById(R.id.txt_number);
        txt_email = header.findViewById(R.id.txt_email);


        txt_user.setText(helper.getUserName());
        txt_number.setText(helper.getPhoneNo());
        txt_email.setText(helper.getGroupName());*/

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, ExploreFragment.newInstance(1)).commit();


    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (doubleBackToExitPressedOnce) {
            finishAffinity();
            return;
        }
        this.doubleBackToExitPressedOnce = true;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_inbox) {
            getSupportActionBar().setTitle(getString(R.string.menu_inbox));
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, InboxFragment.newInstance(1)).commit();
            navigation.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_courses) {
            getSupportActionBar().setTitle(getString(R.string.menu_courses));
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, ExploreFragment.newInstance(1)).commit();
            navigation.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_training) {
            getSupportActionBar().setTitle(getString(R.string.menu_training));
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, TrainingFragment.newInstance(1)).commit();
            navigation.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_services) {
            getSupportActionBar().setTitle(getString(R.string.menu_services));
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, ServicesFragment.newInstance(1)).commit();
            navigation.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_services_digital) {
            startActivity(new Intent(MainActivity.this, DigitalActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
            navigation.setVisibility(View.GONE);
        } else if (id == R.id.nav_services_international) {
            startActivity(new Intent(MainActivity.this, InternationalActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
            navigation.setVisibility(View.GONE);
        } else if (id == R.id.nav_news) {
            startActivity(new Intent(MainActivity.this, NewsActivity.class));
            overridePendingTransition(R.anim.enter, R.anim.exit);
            navigation.setVisibility(View.GONE);

        } else if (id == R.id.nav_help) {
            getSupportActionBar().setTitle(getString(R.string.menu_help_desk));
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, ExploreFragment.newInstance(1)).commit();
            navigation.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_chat) {
            getSupportActionBar().setTitle(getString(R.string.menu_chat_boat));
            navigation.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_notification) {
            getSupportActionBar().setTitle(getString(R.string.menu_notification));
            navigation.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_profile) {
            getSupportActionBar().setTitle(getString(R.string.menu_profile));
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, ProfileFragment.newInstance(1)).commit();
            navigation.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_explore) {
            getSupportActionBar().setTitle(getString(R.string.menu_explore));

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, ExploreFragment.newInstance(1)).commit();
            navigation.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_logout) {
            logoutDialog();
        }

        uncheckNavigationView(id);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(R.string.msg_logout);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                progress.createDialog(false);
                progress.DialogMessage(getString(R.string.msg_logout));
                progress.showDialog();
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {

                        /*try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                            helper.clearAllPrefs();
                            startActivity(new Intent(MainActivity.this, LoginActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            progress.hideDialog();
                            MainActivity.this.finish();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }*/
                        helper.clearAllPrefs();
                        startActivity(new Intent(MainActivity.this, LoginActivity.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        progress.hideDialog();
                        MainActivity.this.finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);

                    }
                });
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void uncheckNavigationView(int id) {
        int size = navigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            if (navigationView.getMenu().getItem(i).getItemId() != id) {
                navigationView.getMenu().getItem(i).setCheckable(false);
            } else {
                navigationView.getMenu().getItem(i).setCheckable(true);
            }
        }

        int size2 = navigation.getMenu().size();
        for (int i = 0; i < size2; i++) {
            if (navigation.getMenu().getItem(i).getItemId() != id) {
                navigation.getMenu().getItem(i).setCheckable(false);
            } else {
                navigation.getMenu().getItem(i).setCheckable(true);
            }
        }
    }
}
