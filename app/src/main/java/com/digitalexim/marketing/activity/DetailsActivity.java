package com.digitalexim.marketing.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.util.PreferenceHelper;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

public class DetailsActivity extends AppCompatActivity implements PaymentResultListener {
    private static final String TAG = DetailsActivity.class.getSimpleName();
    private String money = "";
    private PreferenceHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);


        AppCompatTextView tv_value_money = findViewById(R.id.tv_value_money);
        AppCompatTextView tv = (AppCompatTextView) findViewById(R.id.tv_actual_amount);
        tv.setText("999.00");
        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        AppCompatTextView tv_desc = findViewById(R.id.tv_desc);

        money = tv_value_money.getText().toString();
        helper = new PreferenceHelper(this);

        Button btn_payment = findViewById(R.id.btn_payment);
        btn_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (money.isEmpty() || money == null) {
                    Toast.makeText(DetailsActivity.this, "Payment can't..!!", Toast.LENGTH_SHORT).show();
                } else {
                    startPayment(money);
                }
            }
        });
        tv_desc.setMovementMethod(new ScrollingMovementMethod());
    }

    private void startPayment(String money) {
        final Activity activity = DetailsActivity.this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Razorpay Corp");
            options.put("description", "Demoing Charges");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", money);

            JSONObject preFill = new JSONObject();
            preFill.put("email", helper.getEmailId());
            preFill.put("contact", helper.getPhoneNo());

            options.put("prefill", preFill);

            co.open(activity, options);

            Log.d(TAG, "data options :" + options);
            Log.d(TAG, "data preFill :" + preFill);


        } catch (Exception e) {
            Toast.makeText(DetailsActivity.this, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        Log.d(TAG, "onPaymentSuccess : " + s);

        startActivity(new Intent(DetailsActivity.this, ThankYouActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onPaymentError(int i, String s) {
        Log.d(TAG, "onPaymentError : " + s);

    }
}
