package com.digitalexim.marketing.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.api.ParamsRequest;
import com.digitalexim.marketing.fragment.GetRequest;
import com.digitalexim.marketing.util.AppConfig;
import com.digitalexim.marketing.util.PreferenceHelper;
import com.digitalexim.marketing.util.Progress;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;



public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getSimpleName();
    private TextInputEditText et_email;
    private TextInputEditText et_password;
    private TextInputLayout input_email;
    private TextInputLayout input_password;
    private Progress progress;
    private String email = "", password = "", social_type = "1",
            token = "", phone = "",image = "", emailsp = "" ,
            city = "",name ="",user_id = "";
    protected PreferenceHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

       /* FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        String fcmToken = task.getResult().getToken();
                        Log.e("onComplete: ", fcmToken);
                        Log.d("onComplete: ", fcmToken);
                    }
                });*/
        progress = new Progress(this);
        helper = new PreferenceHelper(this);

        input_email = findViewById(R.id.il_email);
        input_password = findViewById(R.id.il_password);
        et_email = findViewById(R.id.et_email_address);
        et_password = findViewById(R.id.et_password);

    }

    public void onRegister(View view) {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }

    public void onForget(View view) {
//        Toast.makeText(this, "Forget", Toast.LENGTH_SHORT).show();
        openFPAlert();

    }

    private void openFPAlert() {
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.dialog_fp_alert);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.CENTER);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        Button btn_cancel = mBottomSheetDialog.findViewById(R.id.btn_cancel);
        Button btn_send = mBottomSheetDialog.findViewById(R.id.btn_send);
        final EditText et_email = mBottomSheetDialog.findViewById(R.id.et_email);


        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = et_email.getText().toString();

                if (et_email.getText().toString().isEmpty()) {
                    et_email.setError(getString(R.string.msg_error_email));
                    return;
                }

                forgetEmail();

                mBottomSheetDialog.dismiss();
            }
        });


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        mBottomSheetDialog.show();
    }

    private void forgetEmail() {
        HashMap<String, String> params = new HashMap<>();
//        params.put("email", email);
        GetRequest getRequest = new GetRequest(LoginActivity.this, AppConfig.URL.URL_FORGET, params, new GetRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(JSONObject response) {

                try {
                    JSONObject userDetails = response.getJSONObject("data");
                    Log.d(TAG, "userDetails : " + userDetails);
                    Log.d(TAG, "msg : " + userDetails.getString("msg"));
                    Log.d(TAG, "user Profile : " + new GsonBuilder().setPrettyPrinting().create().toJson(response));

                    LayoutInflater inflater = getLayoutInflater();
                    View view1 = inflater.inflate(R.layout.cust_toast_layout,
                            (ViewGroup) findViewById(R.id.relativeLayout1));

                    TextView tv = view1.findViewById(R.id.textView2);
                    tv.setText("Click on link to your email ID for Forget Password..!!");
                    Toast toast = new Toast(LoginActivity.this);
                    toast.setView(view1);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d(TAG, "response : " + response);

            }
        }, false);

        getRequest.doInBackEnd();
    }


    public void onLogo(View view) {
        et_email.setText("chintanprajapati74@gmail.com");
        et_password.setText("Abc@223133");
    }

    public void onLogin(View view) {
//        startActivity(new Intent(LoginActivity.this, MainActivity.class));
//        overridePendingTransition(R.anim.enter, R.anim.exit);

        /*if (!validEmail()) {
            return;
        }

        if (!validPassword()) {
            return;
        }

        RequestBody formBody = new FormBody.Builder()
                .add("email", et_email.getText().toString())
                .add("password", et_password.getText().toString())
                .add("social_type", "0")
                .build();
        new PostRequest(this, URL_LOGIN, formBody, new PostRequest.Callbacks() {
            @Override
            public void onFail(final String error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_LONG).show();

                        *//*LayoutInflater inflater = getLayoutInflater();
                        View view1 = inflater.inflate(R.layout.cust_toast_layout,
                                (ViewGroup) findViewById(R.id.relativeLayout1));

                        TextView tv = view1.findViewById(R.id.textView2);

                        Toast toast = new Toast(LoginActivity.this);
                        toast.setView(view1);
                        toast.setDuration(Toast.LENGTH_LONG);
                        toast.show();*//*

         *//*LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.cust_toast_layout,
                                (ViewGroup) findViewById(R.id.relativeLayout1));
                        TextView text = (TextView) layout.findViewById(R.id.text);
                        text.setText("" + error);

                        Toast toast = new Toast(getApplicationContext());
                        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                        toast.setDuration(Toast.LENGTH_LONG);
                        toast.setView(layout);
                        toast.show();*//*
                    }
                });
            }

            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONObject data = response.getJSONObject("data");
                    JSONObject status_code = response.getJSONObject("status_code");
                    String message = data.getString("message");

                    Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();

                    Log.d(TAG, "data : "+ data);
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute();*/

        if (!validEmail()) {
            return;
        }

        if (!validPassword()) {
            return;
        }

        email = et_email.getText().toString();
        password = et_password.getText().toString();
        login(email, password, social_type);
    }

    private void login(String email, String password, String social_type) {

        HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);
        params.put("social_type", social_type);

        ParamsRequest paramsRequest = new ParamsRequest(this, AppConfig.URL.URL_LOGIN, params, new ParamsRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(LoginActivity.this,  error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(JSONObject response) {

                try {
                    JSONObject userDetails = response.getJSONObject("data");
                    Log.d(TAG , "userDetails : " + userDetails);
                    token =  userDetails.getString("token");
                    name = userDetails.getString("name");
                    image = userDetails.getString("image");
                    emailsp = userDetails.getString("email");
                    user_id = userDetails.getString("user_id");
                    phone = userDetails.getString("phone_number");
                    city = userDetails.getString("city");
                    Log.d(TAG , "token : " + userDetails.getString("token"));
                    Log.d(TAG , "name : " + userDetails.getString("name"));
                    Log.d(TAG , "image : " + userDetails.getString("image"));
                    Log.d(TAG , "email : " + userDetails.getString("email"));
                    Log.d(TAG , "phone_number : " + userDetails.getString("phone_number"));
                    Log.d(TAG , "city : " + userDetails.getString("city"));
                    Log.d(TAG, "userDetails : " + new GsonBuilder().setPrettyPrinting().create().toJson(response));


                    LayoutInflater inflater = getLayoutInflater();
                    View view1 = inflater.inflate(R.layout.cust_toast_layout,
                            (ViewGroup) findViewById(R.id.relativeLayout1));

                    TextView tv = view1.findViewById(R.id.textView2);
                    tv.setText(response.getString("message"));
                    Toast toast = new Toast(LoginActivity.this);
                    toast.setView(view1);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.show();

                    helper.setToken(token);
                    helper.setUserName(name);
                    helper.setImage(image);
                    helper.setEmailId(emailsp);
                    helper.setUserId(user_id);
                    helper.setPhoneNo(phone);
                    helper.setLogin(true);

                    //main activity
                   /* Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);*/

                    Intent intent = new Intent(LoginActivity.this, ActChooseCategory.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, true);

        paramsRequest.doInBackEnd();
    }


    private boolean validEmail() {
        String email = et_email.getText().toString().trim();
        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            input_email.setError("Please Enter Valid Email ID");
            return false;
        } else {
            return true;
        }
    }

    private boolean validPassword() {
        String password = et_password.getText().toString().trim();
        if (password.isEmpty()) {
            input_password.setError("Please Enter Valid Password");
            return false;
        } else {
            password.length();
            return true;
        }
    }

    public void onGoogle(View view) {

    }

    public void onFB(View view) {

    }

    public void onLinked(View view) {
    }

    public void onTerms(View view) {
        startActivity(new Intent(LoginActivity.this, TermsActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }

    public void onBack(View view) {
        finish();
    }


}
