package com.digitalexim.marketing.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.digitalexim.marketing.ActBase;
import com.digitalexim.marketing.R;
import com.digitalexim.marketing.util.Constants;
import com.orhanobut.hawk.Hawk;

public class WelcomActivity extends ActBase {
    private static final String TAG = WelcomActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcom);

        Log.d(TAG, "welcome  : " + Hawk.contains(Constants.SHAREDPREFERENCE.USER.show_intro));
    }

    public void onWelcom(View view) {
        startActivity(new Intent(WelcomActivity.this, LoginActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }
}
