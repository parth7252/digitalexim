package com.digitalexim.marketing.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.util.Constants;
import com.orhanobut.hawk.Hawk;

public class ActChooseCategory extends AppCompatActivity implements View.OnClickListener {
    Button btn_digital_exim,btn_import_export;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_choose_category);
        init();
        setClickEvent();
    }

    private void setClickEvent() {
        btn_digital_exim.setOnClickListener(this);
        btn_import_export.setOnClickListener(this);
    }

    private void init() {
        btn_digital_exim = findViewById(R.id.btn_digital_exim);
        btn_import_export = findViewById(R.id.btn_digital_exim);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_digital_exim:
                Hawk.put(Constants.SHAREDPREFERENCE.USER.user_category, Constants.SHAREDPREFERENCE.USER.category_digital_exim);
                setHomeIntent();
                break;
            case R.id.btn_import_export:
                Hawk.put(Constants.SHAREDPREFERENCE.USER.user_category, Constants.SHAREDPREFERENCE.USER.category_import_export);
                setHomeIntent();
                break;



        }


    }

    @Override
    public void onBackPressed() {
      //  super.onBackPressed();
        Hawk.put(Constants.SHAREDPREFERENCE.USER.user_category, Constants.SHAREDPREFERENCE.USER.category_digital_exim);
        setHomeIntent();
    }

    private void setHomeIntent(){
        Intent intent = new Intent(ActChooseCategory.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }
}
