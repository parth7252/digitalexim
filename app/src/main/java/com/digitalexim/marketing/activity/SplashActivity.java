package com.digitalexim.marketing.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.util.Constants;
import com.digitalexim.marketing.util.PreferenceHelper;
import com.orhanobut.hawk.Hawk;

public class SplashActivity extends AppCompatActivity {
    private PreferenceHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        helper = new PreferenceHelper(getApplicationContext());

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d("notification splash", "Key: " + key + " Value: " + value);
            }

        }

        splash();
    }

    private void splash(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!helper.isLogin()) {
                    startActivity(new Intent(SplashActivity.this, WelcomActivity.class));
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                }else{
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                finish();

                Log.d("SplashActivity", "splash : " +Hawk.contains(Constants.SHAREDPREFERENCE.USER.show_intro));
            }
        }, 2000);
    }
}
