package com.digitalexim.marketing.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.fragment.Fragment_Walkthrough1;
import com.digitalexim.marketing.fragment.Fragment_Walkthrough2;
import com.digitalexim.marketing.fragment.Fragment_Walkthrough3;
import com.digitalexim.marketing.util.Constants;
import com.orhanobut.hawk.Hawk;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActIntroSlider extends AppCompatActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.next)
    LinearLayout next;


    WalkThroughAdapter viewPagerAdapter;

    @BindView(R.id.indicator)
    WormDotsIndicator indicators;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (Build.VERSION.SDK_INT < 16) {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else {
                View decorView = getWindow().getDecorView();
                // Hide the status bar.

                int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                decorView.setSystemUiVisibility(uiOptions);
                // Remember that you should never show the action bar if the
                // status bar is hidden, so hide that too if necessary.

            }
            setContentView(R.layout.activity_act_intro_slider);
            // ViewGroup.inflate(this, R.layout.act_intro_slider, ll_SubMainContainer);

            ButterKnife.bind(this);

            initialize();


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initialize() {

        Hawk.put(Constants.SHAREDPREFERENCE.USER.show_intro, "1");


        viewPagerAdapter = new WalkThroughAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        indicators.setViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 2){
                    next.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewPager.getCurrentItem() == 2) {

                    Intent i1 = new Intent(ActIntroSlider.this, WelcomActivity.class);
                    finish();
                    startActivity(i1);
                }

            }
        });


    }

    public void onNext(View view) {
        Intent i1 = new Intent(ActIntroSlider.this, WelcomActivity.class);
        finish();
        startActivity(i1);
    }


    private class WalkThroughAdapter extends FragmentPagerAdapter {

        public WalkThroughAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {

            switch (i) {
                case 0:
                    Fragment_Walkthrough1 firstScreen = new Fragment_Walkthrough1();
                    return firstScreen;

                case 1:
                    Fragment_Walkthrough2 secaondScreen = new Fragment_Walkthrough2();
                    return secaondScreen;

                case 2:
                    Fragment_Walkthrough3 thirdScreen = new Fragment_Walkthrough3();
                    return thirdScreen;
            }

            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
