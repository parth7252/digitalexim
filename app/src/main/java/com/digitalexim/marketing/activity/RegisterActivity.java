package com.digitalexim.marketing.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.digitalexim.marketing.R;
import com.digitalexim.marketing.api.ParamsRequest;
import com.digitalexim.marketing.util.AppConfig;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private TextInputEditText et_email_address, et_phone, et_city, et_full_name, et_password;
    private TextInputLayout il_full_name, il_email, il_password, il_phone , il_city;
    private String social_type = "1", email = "", phone = "", fullname = "", city = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        et_full_name = findViewById(R.id.et_full_name);
        et_email_address = findViewById(R.id.et_email_address);
        et_phone = findViewById(R.id.et_phone);
        et_city = findViewById(R.id.et_city);
        il_city = findViewById(R.id.il_city);
        il_full_name = findViewById(R.id.il_full_name);
        il_email = findViewById(R.id.il_email);
        il_phone = findViewById(R.id.il_phone);
        il_password = findViewById(R.id.il_password);
        et_password = findViewById(R.id.et_password);
    }

    public void onSubmit(View view) {
        social_type = "1";
        email = et_email_address.getText().toString();
        fullname = et_full_name.getText().toString();
        phone = et_phone.getText().toString();
        city = et_city.getText().toString();


        if (!fullName()) {
            return;
        }

        if (!phoneNumber()) {
            return;
        }

        if (!validEmail()) {
            return;
        }

        if (!cities()) {
            return;
        }

        if (!validPassword()) {
            return;
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("first_name", fullname);
        params.put("email", email);
        params.put("phone_number", phone);
        params.put("city", city);
        params.put("social_type", social_type);

        ParamsRequest paramsRequest = new ParamsRequest(this, AppConfig.URL.URL_REGISTER, params, new ParamsRequest.Callbacks() {
            @Override
            public void onFail(String error) {
                Toast.makeText(RegisterActivity.this, error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(JSONObject response) {

                try {
                    JSONObject userDetails = response.getJSONObject("data");
                    Log.d(TAG, "userDetails : " + userDetails);
                    LayoutInflater inflater = getLayoutInflater();
                    View view1 = inflater.inflate(R.layout.cust_toast_layout,
                            (ViewGroup) findViewById(R.id.relativeLayout1));

                    TextView tv = view1.findViewById(R.id.textView2);
                    tv.setText(response.getString("message"));
                    Toast toast = new Toast(RegisterActivity.this);
                    toast.setView(view1);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.show();

                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, true);

        paramsRequest.doInBackEnd();
    }

    private boolean cities() {
        String cities = et_city.getText().toString().trim();
        if (cities.isEmpty() || cities == null) {
            il_city.setError("Please Enter City");
            return false;
        } else {
            return true;
        }
    }

    private boolean phoneNumber() {
        String phonenum = et_phone.getText().toString().trim();
        if (phonenum.isEmpty() || phonenum == null) {
            il_phone.setError("Please Enter Phone Number");
            return false;
        } else {
            return true;
        }
    }

    private boolean fullName() {
        String fullname = et_full_name.getText().toString().trim();
        if (fullname.isEmpty() || fullname == null) {
            il_full_name.setError("Please Enter Full Name");
            return false;
        } else {
            return true;
        }
    }

    private boolean validEmail() {
        String email = et_email_address.getText().toString().trim();
        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            il_email.setError("Please Enter Valid Email ID");
            return false;
        } else {
            return true;
        }
    }

    private boolean validPassword() {
        String password = et_password.getText().toString().trim();
        if (password.isEmpty()) {
            il_password.setError("Please Enter Valid Password");
            return false;
        } else {
            password.length();
            return true;
        }
    }

    public void onBack(View view) {
        finish();
    }
}
