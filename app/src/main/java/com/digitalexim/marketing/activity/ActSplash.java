package com.digitalexim.marketing.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.digitalexim.marketing.ActBase;
import com.digitalexim.marketing.R;
import com.digitalexim.marketing.util.App;
import com.digitalexim.marketing.util.Constants;
import com.digitalexim.marketing.util.PreferenceHelper;
import com.orhanobut.hawk.Hawk;

import butterknife.ButterKnife;

public class ActSplash extends ActBase {
    String TAG = "ActSplash";
    int TIME = 3000;
    private PreferenceHelper helper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_act_splash);

        helper = new PreferenceHelper(getApplicationContext());

        try {
            ViewGroup.inflate(this, R.layout.act_splash, ll_SubMainContainer);
            ButterKnife.bind(this);
            initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initialize() {
        try {

            /*----- ActBase -----*/
            rl_baseToolbar.setVisibility(View.GONE);


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    Log.d(TAG, "isLogin : " + !helper.isLogin());
                    Log.d(TAG, "show intro : " + Hawk.contains(Constants.SHAREDPREFERENCE.USER.show_intro));

                    if (!Hawk.contains(Constants.SHAREDPREFERENCE.USER.show_intro)) {
//                        Toast.makeText(ActSplash.this, "Show intro first time", Toast.LENGTH_SHORT).show();
                        Intent i1 = new Intent(ActSplash.this, ActIntroSlider.class);
                        App.myStartActivityRefersh(ActSplash.this, i1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (!helper.isLogin()) {
//                        Toast.makeText(ActSplash.this, "Welcome activity", Toast.LENGTH_SHORT).show();
                        Intent i1 = new Intent(ActSplash.this, WelcomActivity.class);
                        App.myStartActivityRefersh(ActSplash.this, i1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else {
//                        Toast.makeText(ActSplash.this, "MainActivity", Toast.LENGTH_SHORT).show();
                        Intent i1 = new Intent(ActSplash.this, MainActivity.class);
                        App.myStartActivityRefersh(ActSplash.this, i1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    }


                    /*if (Hawk.contains(Constants.SHAREDPREFERENCE.USER.show_intro) &&
                            Hawk.get(Constants.SHAREDPREFERENCE.USER.show_intro).toString().equalsIgnoreCase("1")) {
                        Intent i1 = new Intent(ActSplash.this, WelcomActivity.class);
                        App.myStartActivityRefersh(ActSplash.this, i1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);

                    }

                    else {

                        Intent i1 = new Intent(ActSplash.this, ActIntroSlider.class);
                        App.myStartActivityRefersh(ActSplash.this, i1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);

                    }*/
                }
            }, TIME);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
