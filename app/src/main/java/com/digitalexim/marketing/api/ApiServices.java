package com.digitalexim.marketing.api;

import com.digitalexim.marketing.api.model.GetCourseResponse;
import com.digitalexim.marketing.model.CommonResponse;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

public interface ApiServices {
    @Multipart
    @POST("logout")
    Call<CommonResponse> logout(
            @PartMap() Map<String, RequestBody> partMap
    );

    @Multipart
    @POST("logout")
    Call<GetCourseResponse> getCourseList(
            @PartMap() Map<String, RequestBody> partMap
    );



}
