package com.digitalexim.marketing.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CourseListModel {

    @SerializedName("course_title")
    @Expose
    public String courseTitle;
    @SerializedName("course_duration")
    @Expose
    public String courseDuration;
    @SerializedName("course_feature_image")
    @Expose
    public String courseFeatureImage;
    @SerializedName("course_description")
    @Expose
    public String courseDescription;
    @SerializedName("course_type")
    @Expose
    public String courseType;
    @SerializedName("course_category")
    @Expose
    public String courseCategory;
    @SerializedName("select_course")
    @Expose
    public String selectCourse;
    @SerializedName("Lession_description")
    @Expose
    public List<LessionDescriptionModel> lessionDescription = null;
}
