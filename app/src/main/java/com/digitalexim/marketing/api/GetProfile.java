package com.digitalexim.marketing.api;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.digitalexim.marketing.fragment.GetRequest;
import com.digitalexim.marketing.util.App;
import com.digitalexim.marketing.util.PD;
import com.digitalexim.marketing.util.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class GetProfile {
    private static final String TAG = GetRequest.class.getSimpleName();
    private String mUrl;
    private GetProfile.Callbacks mCallbacks;
    private Activity context;
    private PreferenceHelper helper;
    private PD pd;
    private HashMap<String, String> params;

    public GetProfile(FragmentActivity activity, String url, HashMap<String, String> params, GetProfile.Callbacks callbacks, boolean isProgress) {
        this.mUrl = url;
        this.context = activity;
        this.mCallbacks =  callbacks;
        this.params = params;
        helper = new PreferenceHelper(context);
        pd = new PD(context);

        if (isProgress) {
            pd.createDialog();
            pd.show();
        }
    }




    public void doInBackEnd() {


        StringRequest strReq = new StringRequest(Request.Method.GET, "http://digitalexim.com/digital_exim/api/user_profile?user_id=" + helper.getUserId(), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                dismissDialog();
//                Log.d(TAG, "url : " + mUrl);
//                Log.d(TAG, "response : " + response);
//                Log.d(TAG, "main : " + App.getInstance().BASE_URL + mUrl);
                Log.d(TAG, "userid : " + helper.getUserId());
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status_code").equals("1")) {
                        mCallbacks.onSuccess(jsonObject);
                    }
                    else {
                        mCallbacks.onFail(jsonObject.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("onError ", mUrl + error.getMessage());
                dismissDialog();
                mCallbacks.onFail(error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
//                headers.put(Params.Accept, accept);
//                headers.put(Params.Authorization, "Bearer "+ helper.LoadStringPref(Prefs.token, ""));
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };

        App.getInstance().addToRequestQueue(strReq, "string_req");

    }

    public interface Callbacks {
        void onFail(String error);

        void onSuccess(JSONObject response);
    }

    private void logoutDialog(final Context context) {

    }

    private void dismissDialog() {
        context.runOnUiThread(new Runnable() {
            @SuppressLint("NewApi")
            @Override
            public void run() {
                if (!context.isDestroyed() && !context.isFinishing()) {
                    pd.hide();
                }
            }
        });
    }
}
