package com.digitalexim.marketing.api;

import com.digitalexim.marketing.response.RespLogin;
import com.digitalexim.marketing.util.AppConfig;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @Multipart
    @POST(AppConfig.URL.URL_LOGIN)
    Call<RespLogin> LOGIN(@Part("email") RequestBody email,
                          @Part("password") RequestBody password,
                          @Part("social_type") RequestBody social_type);

}
