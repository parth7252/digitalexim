package com.digitalexim.marketing.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LessionDescriptionModel {

    @SerializedName("lession_title")
    @Expose
    public String lessionTitle;
    @SerializedName("lession_video")
    @Expose
    public String lessionVideo;
    @SerializedName("lession_description")
    @Expose
    public String lessionDescription;
}
