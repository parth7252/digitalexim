package com.digitalexim.marketing.api;

public class ApiFunction {

    public static String strBaseUrl = "http://eliteinfoworld.net/kdahpro2019/public/api/v1.0.1/"; //live



    public static String strMP3 = "http://www.lohanaconnect.com/images/dhvjvandan_song.mp3";


    public static int strLogout = 202;
    public static String strEmialNotVerify = "5";
    public static String strMobileNotVerify = "6";
    public static String strUserNotActive = "3";
    public static String strUserRejected = "2";
    public static String strINDIANCOUNTRY = "+91";
    public static boolean strAPITRUE = true;
    public static boolean strAPIFALSE = false;


    //-- parameters
    public static String
            PARM_FULLNAME = "fullname",
            PARM_EMAIL = "email",
            PARM_MOBILE = "mobile",
            PARM_OTP = "otp",
            PARM_PASSWORD = "password",
            PARM_FBID = "fbid",
            PARM_GOOGLEID = "gid",
            PARM_PHOTO = "photo",
            PARM_PLATFORM = "platform",
            PARM_APPMODE = "app_mode",
            PARM_ANDROIDTOKEN = "device_token",
            PARM_IOSTOKEN = "iostoken",
            PARM_APPVERSION = "appversion",
            PARM_USERID = "user_id",
            PARM_ACCESSTOKEN = "access_token",
            PARM_EVENT_ID = "event_id",
            PARM_SUBJECT = "subject",
            PARM_QUESTION = "question",
            PARM_GENDER = "gender",
            PARM_BIRTHDATE = "birthdate",
            PARM_LANGUAGE = "language",
            PARM_BUSINESS_NAME = "business_name",
            PARM_BUSINESS_ADD_1 = "business_add1",
            PARM_BUSINESS_ADD_2 = "business_add2",
            PARM_COUNTRY = "countryListModel",
            PARM_CITY = "city",
            PARM_STATE = "state",
            PARM_ABOUT_BUSINESS = "about_business",
            PARM_CATEGORY = "category",
            PARM_SUB_CATEGORY = "sub_category",
            PARM_WEBSITE = "website",
            PARM_USER_PHOTO = "user_photo",
            PARM_BUSINESS_CARD = "business_card",
            PARM_KYC = "kyc",
            PARM_BUSINESS_PHOTO = "business_photo",
            PARM_BUSINESS_ID = "business_id",
            PARM_CATEGORY_ID = "category_id",
            PARM_COUNTRY_ID = "country_id",
            PARM_STATE_ID = "state_id",
            PARM_PRIVACY_POLICY = "privacy_policy",
            PARM_TERMS_CONDITION = "terms_condition",
            PARM_ABOUT_LOHANA_CONNECT = "about_lohana_connect",
            PARM_ABOUT_SHREE_LOHANA_MAHAPARISAD = "about_shree_lohana_mahaparisad",
            PARMSLUG = "slug",
            PARMS_START_DATE = "start_date",
            PARMS_END_DATE = "end_date",
            PARMS_NOTE = "notes",
            PARMS_LATITUDE= "latitude",
            PARMS_LOGITUDE = "longitude",
            PARMS_DEVICE_INFO= "device_unique_id",
            PARMS_APP_VERSION = "app_version",
            PARMS_CONFIRM_PASSWORD = "confirm_password",
            PARMS_PAGE = "page",
            PARMS_OP = "op",
            PARMS_IO = "io";

}
